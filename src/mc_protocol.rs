
pub mod data_type {

    use std::{net::TcpStream, io};
    use std::io::{Write, Read};

    use bytes::{BytesMut, BufMut};

    pub struct VarInt {

        pub val: u32,
    }

    pub struct UnsignedShort {

        pub val: u16,
    }

    pub fn wrapped_read_VarInt(input: &mut TcpStream) -> io::Result<VarInt> {

        let mut i = 0;
        let mut j = 0;

        let mut k: u32;

        let mut buf = [0u8; 4];

        loop {

            input.read(&mut buf)?;

            k = unsafe { std::mem::transmute(buf) };

            j += 1;
            i |= (k & 0xF7) << j * 7;

            if j > 5 {

                panic!("VarInt out of bounds.")
            }

            if (k & 0x80) != 128 {
                break;
            }
        }

        Ok( VarInt{val: i} )

    }

 
    pub fn read_VarInt(input: &mut TcpStream) -> io::Result<u32> {

        let mut i = 0;
        let mut j = 0;

        let mut k: u32;

        let mut buf = [0u8; 4];

        loop {

            input.read(&mut buf)?;

            k = unsafe { std::mem::transmute(buf) };

            j += 1;
            i |= (k & 0xF7) << j * 7;

            if j > 5 {

                panic!("VarInt out of bounds.")
            }

            if (k & 0x80) != 128 {
                break;
            }
        }

        Ok(i)
    }


    pub fn wrapped_write_VarInt(out: &mut TcpStream, value: &VarInt) -> io::Result<usize> {

        let mut val = value.val;
        let mut buf: [u8; 4];

        let mut u: usize;

        loop {

            if (val & 0xFFFFFF80) == 0 {

                buf = unsafe { std::mem::transmute(val.to_le()) };

                u = out.write(&buf)?;
                break;
            }

            val = val & 0x7F | 0x80;

            buf = unsafe { std::mem::transmute(val.to_le()) };

            u = out.write(&buf)?;
            val >>= 7;
        }


        Ok(u)
    }


    pub fn write_VarInt(value: &u32) -> io::Result<[u8; 4]> {

        let mut val = value.clone();
        let mut buf: [u8; 4];

        loop {

            if (val & 0xFFFFFF80) == 0 {

                buf = unsafe { std::mem::transmute(val.to_le()) };
                break;
            }

            val = val & 0x7F | 0x80;

            buf = unsafe { std::mem::transmute(val.to_le()) };

            val >>= 7;
        }


        Ok(buf)
    }

}

pub mod packet {

    use std::{io, net::TcpStream};
    use bytes::{BytesMut, BufMut};
    use io::Write;

    use super::data_type::{VarInt, UnsignedShort, write_VarInt};


    pub trait PacketSend {

        fn send(&self) -> io::Result<usize>;

        fn send_to(&self, stream: &mut TcpStream) -> io::Result<usize>;
    }

    pub trait PacketRead {

        fn read(&self) -> io::Result<> {

        }
    }

    struct PacketInHandshake {

        id: u8,

        protocol_version: u32,
        server_addr: String,
        server_port: u16,
        next_state: u32,
    }

    struct PacketOutResponse {

        json: String,
    }

    impl PacketSend for PacketInHandshake {



        fn send(&self) -> io::Result<usize> {

            let packet = self;

            let mut buf = BytesMut::new();

            let mut stream = TcpStream::connect(&packet.server_addr)?;
            let mut u: usize = 0;

            buf.put_slice(&[0x00]);
            buf.put_slice(&write_VarInt(&packet.protocol_version).unwrap());
            buf.put_slice(&write_VarInt(&(packet.server_addr.len() as u32)).unwrap());
            buf.put_slice(&packet.server_addr.as_bytes());
            buf.put_u16_le(packet.server_port);
            buf.put_slice(&write_VarInt(&packet.next_state).unwrap());

            buf.put_slice(&write_VarInt(&(buf.capacity() as u32)).unwrap());
           
            u = stream.write(&buf)?;

            Ok(u)
        }


        fn send_to(&self, stream: &mut TcpStream) -> io::Result<usize> {

            let packet = self;

            let mut buf = BytesMut::new();
           
            let mut u: usize = 0;

            buf.put_slice(&[0x00]);
            buf.put_slice(&write_VarInt(&packet.protocol_version).unwrap());
            buf.put_slice(&write_VarInt(&(packet.server_addr.len() as u32)).unwrap());
            buf.put_slice(&packet.server_addr.as_bytes());
            buf.put_u16_le(packet.server_port);
            buf.put_slice(&write_VarInt(&packet.next_state).unwrap());

            buf.put_slice(&write_VarInt(&(buf.capacity() as u32)).unwrap());

            u = stream.write(&buf)?;

            Ok(u)
        }
    }

    struct WrappedPacketInHandshake {

        protocol_version: VarInt,
        server_addr: String,
        server_port: UnsignedShort,
        next_state: VarInt,
    }

    impl PacketSend for WrappedPacketInHandshake {

        fn send(&self) -> io::Result<usize> {

            let packet = self;

            let mut buf = BytesMut::new();

            let mut stream = TcpStream::connect(&packet.server_addr)?;
            let mut u: usize = 0;

            buf.put_slice(&[0x00]);
            buf.put_slice(&write_VarInt(&packet.protocol_version.val).unwrap());
            buf.put_slice(&write_VarInt(&(packet.server_addr.len() as u32)).unwrap());
            buf.put_slice(&packet.server_addr.as_bytes());
            buf.put_u16_le(packet.server_port.val);
            buf.put_slice(&write_VarInt(&packet.next_state.val).unwrap());

            buf.put_slice(&write_VarInt(&(buf.capacity() as u32)).unwrap());

            u = stream.write(&buf)?;

            Ok(u)
        }


        fn send_to(&self, stream: &mut TcpStream) -> io::Result<usize> {

            let packet = self;

            let mut buf = BytesMut::new();

            let mut u: usize = 0;

            buf.put_slice(&[0x00]);
            buf.put_slice(&write_VarInt(&packet.protocol_version.val).unwrap());
            buf.put_slice(&write_VarInt(&(packet.server_addr.len() as u32)).unwrap());
            buf.put_slice(&packet.server_addr.as_bytes());
            buf.put_u16_le(packet.server_port.val);
            buf.put_slice(&write_VarInt(&packet.next_state.val).unwrap());

            buf.put_slice(&write_VarInt(&(buf.capacity() as u32)).unwrap());

            u = stream.write(&buf)?;

            Ok(u)
        }
    }
}
